using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemData : MonoBehaviour {

	public struct ItemDetails {
		
		public int itemId;
		public string itemName;
		public string itemFormattedPrice;
		
		public ItemDetails(int id, string name, string formattedPrice) {
			itemId = id;
			itemName = name;
			itemFormattedPrice = formattedPrice;
		}
	}
	
	public Dictionary<int,ItemDetails> itemDetails;
	
	void Awake () {
		itemDetails = new Dictionary<int, ItemDetails>();
		itemDetails.Add(0, new ItemDetails(0, "Bus Pass", "$0.99"));
	}
}
