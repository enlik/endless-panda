using UnityEngine;
using System.Collections;

public class CameraAdjust : MonoBehaviour {

	public float displayWidth;
	public float displayHeight;
	
	void Start () {
		displayWidth = Screen.width;
		displayHeight = Screen.height;
		
//		if(displayWidth/displayHeight >= 1.6f) { //if 16:9 or more
//			this.GetComponent<Camera>().orthographicSize = 90;
//		}
//		else if(displayWidth/displayHeight > 1.4f) { //if 3:2 resolution or more
//			this.GetComponent<Camera>().orthographicSize = 100;
//		}
//		else{ //if less than 3:2 resolution
//			this.GetComponent<Camera>().orthographicSize = 100;
//		}
		
		
		if(displayWidth/displayHeight >= 1.6f) { //if 16:9 or more
			this.GetComponent<Camera>().orthographicSize = 170;
		}
		else if(displayWidth/displayHeight > 1.3f) { //if 3:2 resolution or more
			this.GetComponent<Camera>().orthographicSize = 150;
		}
		else{ //if less than 3:2 resolution
			this.GetComponent<Camera>().orthographicSize = 150;
		}
		
	}
}
