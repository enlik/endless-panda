using UnityEngine;
using System.Collections;

[RequireComponent(typeof(tk2dTextMesh))]
public class LanguageCustomizer : MonoBehaviour {
	public string key; //Localization key
	public tk2dFontData fontOverride;
	public tk2dFontData[] fontSets;
	public Vector3[] relativePositions;
	public Vector3[] fontSizes;
	
	Vector3 originalPosition;
	string mLanguage;
	bool mStarted = false;
	
	void Start () {
		if (Localization.instance == null) return;
		
		originalPosition = transform.localPosition;
		if(fontOverride == null) {
			fontSets = new tk2dFontData[Localization.instance.fonts.Length];
			relativePositions = new Vector3[fontSets.Length];
			fontSizes = new Vector3[fontSets.Length];
			for(int i = 0; i < fontSets.Length; i++) {
				fontSets[i] = Localization.instance.fonts[i];
				if(i == 0) {
					fontSizes[i] = this.gameObject.transform.localScale;
				}
			}
		}
		else {
			fontSets = new tk2dFontData[1];
			fontSets[0] = fontOverride;
			relativePositions = new Vector3[1];
			fontSizes = new Vector3[1];
			fontSizes[0] = this.gameObject.transform.localScale;
		}
		mStarted = true;
		if (Localization.instance != null) OnLocalize(Localization.instance);
	}
	
	/// <summary>
	/// Localize the widget on enable, but only if it has been started already.
	/// </summary>

	void OnEnable ()
	{
		//EventsManager.onLocalize += OnLocalize;
		
		if (mStarted && Localization.instance != null){
			OnLocalize(Localization.instance);
		}
	}
	
	void OnDisable(){
		//EventsManager.onLocalize -= OnLocalize;
	}

	/// <summary>
	/// Localize the widget on start.
	/// </summary>
	
	int FindLanguageIndex(string languageName, Localization loc)
	{
		for (int i=0; i<loc.languages.Length; i++){
			if(languageName == loc.languages[i].name){
				return i;
			}
		}
		
		return -1;
	}
	
	public void RefreshWidget(){
		tk2dTextMesh lbl = GetComponent<tk2dTextMesh>();
		
		// If there's not key, return nothing
		string val = string.IsNullOrEmpty(key) ? "" : Localization.instance.Get(key);
		
		if (lbl != null){
			lbl.text = val.Replace("\\n", "\n");
			lbl.Commit();
		}
	}
	
	/// <summary>
	/// This function is called by the Localization manager via a broadcast SendMessage.
	/// </summary>
	void OnLocalize (Localization loc)
	{
		if (mLanguage != loc.currentLanguage)
		{
			tk2dTextMesh lbl = GetComponent<tk2dTextMesh>();

			// If no localization key has been specified, use the label's text as the key
			if (string.IsNullOrEmpty(mLanguage) && string.IsNullOrEmpty(key) && lbl != null) key = lbl.text;

			// If we still don't have a key, return nothing
			string val = string.IsNullOrEmpty(key) ? "" : loc.Get(key);
			
			// Assign the current language
			mLanguage = loc.currentLanguage;
			
			if (lbl != null)
			{
				lbl.text = val.Replace("\\n", "\n");
				
				// Adjust the text font, size, and position
				int languageIndex = FindLanguageIndex(mLanguage, loc);
				
				if (languageIndex != -1){
					if (languageIndex < fontSets.Length && fontSets[languageIndex] != null){
						lbl.font = fontSets[languageIndex];
					}
					
					if (languageIndex < fontSizes.Length){
						lbl.transform.localScale = fontSizes[languageIndex];
					}
					
					if (languageIndex < relativePositions.Length){
						lbl.transform.localPosition = originalPosition + relativePositions[languageIndex];
					}
				}
			}
			
			lbl.Commit();
		}
	}
}
