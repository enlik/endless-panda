using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : MonoBehaviour {
	
	private static GameData _instance;

	public static GameData Instance {
		get {
			return _instance;
		}
	}	
	
	public List<int> unlockedLevels = new List<int>();
	public List<int> completedLevels = new List<int>();
	public int completedLevelsCounter;
	public int chocolateEventCounter;
	public int currentLevel;
	public int currentWorld;
	
	public int playerCoin;
	
	public List<int> unlockedWorlds = new List<int>();
	
	public List<int> socialRewards = new List<int>(); //1 = facebook, 2 = twitter, 3 = rate
	public int currentSkin;
	
	public List<string> unlockedChars = new List<string>();
	public List<string> boughtChars = new List<string>();
	
	public string currentChar;
	
	public int cutsceneId;
	
	public bool optionMusicIsOn;
	public bool optionSoundIsOn;
	public bool isLoginKakao;
	public string kakaoUserId = string.Empty;
	public string kakaoUserName = string.Empty;
	
	public List<int> achievementRewards = new List<int>();
	public int totalDeaths;

	void Awake () {
		_instance = this;
		
		
//		UnlockAll(); //instantly unlocks and completes all levels
//		UnlockSome(0, 30);
		LoadData();
		if(optionMusicIsOn) {
			AudioController.SetCategoryVolume("Music", 1f);
		}
		else {
			AudioController.SetCategoryVolume("Music", 0f);
		}
		if(optionSoundIsOn) {
			AudioController.SetCategoryVolume("Sound", 1f);
		}
		else {
			AudioController.SetCategoryVolume("Sound", 0f);
		}
	}
	
	void OnEnable () {
		EventsManager.onDataSaveDataE += OnDataSaveData;
		EventsManager.onDataLoadDataE += OnDataLoadData;
	}
	
	void OnDisable () {
		EventsManager.onDataSaveDataE -= OnDataSaveData;
		EventsManager.onDataLoadDataE -= OnDataLoadData;
	}
	
	void Start () {

	}
	
	public void UnlockAll () {
		completedLevels.Add(0);
		unlockedLevels.Add(0);
		for(int i = 0; i < 90; i++) {
			if(!completedLevels.Contains(i+1)) {
				completedLevels.Add(i+1);
			}
			if(!unlockedLevels.Contains(i+1)) {
				unlockedLevels.Add(i+1);
			}
		}
		unlockedWorlds.Add(2);
		unlockedWorlds.Add(3);
//		isLoginKakao = true;
	}
	
	public void UnlockSome (int start, int finish) {
		for(int i = start; i < finish+1; i++) {
			if(!completedLevels.Contains(i)) {
				completedLevels.Add(i);
			}
			if(!unlockedLevels.Contains(i)) {
				unlockedLevels.Add(i);
			}
		}
	}
	
	public void ResetData (){
		unlockedLevels.Clear();
		completedLevels.Clear();
		currentLevel = 1;
		currentWorld = 1;
		playerCoin = 0;
		unlockedWorlds.Clear();
		unlockedChars.Clear();
		boughtChars.Clear();
		currentChar = "apeach";
		
		optionMusicIsOn = true;
		optionSoundIsOn = true;
		
		isLoginKakao = false;
	}
	
	void OnDataSaveData () {
		SaveData();
	}
	
	void OnDataLoadData () {
		LoadData();
	}
	
	void SaveData () {
		if (kakaoUserId == null) kakaoUserId = string.Empty;
		if (kakaoUserName == null) kakaoUserName = string.Empty;
		try {
			Debug.Log("Saving Gameplay Data");
			ES2.Save(unlockedLevels, "unlockedLevels");
			ES2.Save(completedLevels, "completedLevels");
			ES2.Save(completedLevelsCounter, "completedLevelsCounter");
			ES2.Save(chocolateEventCounter, "chocolateEventCounter");
			ES2.Save(unlockedWorlds, "unlockedWorlds");
			ES2.Save(currentLevel, "lastPlayedLevel");
			ES2.Save(currentWorld, "lastPlayedWorld");
			
			//enlik
			ES2.Save(playerCoin, "playerCoin");
			ES2.Save(unlockedChars, "unlockedChars");
			ES2.Save(boughtChars, "boughtChars");
			ES2.Save(currentChar, "currentChar");
			
			ES2.Save(currentSkin, "currentSkin");
			ES2.Save(socialRewards, "socialRewards");
			ES2.Save(optionMusicIsOn, "music");
			ES2.Save(optionSoundIsOn, "sound");
			ES2.Save(isLoginKakao, "isLoginKakao");
			ES2.Save(kakaoUserId, "kakaoUserId");
			ES2.Save(kakaoUserName, "kakaoUserName");
			
			ES2.Save(achievementRewards, "achievementRewards");
			ES2.Save(totalDeaths, "totalDeaths");
			
		}
		catch (ES2Exception ex) {
			Debug.LogError(ex.Message);
			return;
		}
	}
	
	public void LoadData () {
		try {
			if(ES2.Exists("unlockedLevels")) {
				unlockedLevels = ES2.LoadList<int>("unlockedLevels");
			}
			if(ES2.Exists("completedLevels")) {
				completedLevels = ES2.LoadList<int>("completedLevels");
			}
			if(ES2.Exists("completedLevelsCounter")){
				completedLevelsCounter = ES2.Load<int>("completedLevelsCounter");
			}
			if(ES2.Exists("chocolateEventCounter")){
				chocolateEventCounter = ES2.Load<int>("chocolateEventCounter");
			}
			if(ES2.Exists("unlockedWorlds")) {
				unlockedWorlds = ES2.LoadList<int>("unlockedWorlds");
			}
			if(ES2.Exists("lastPlayedLevel")) {
				currentLevel = ES2.Load<int>("lastPlayedLevel");
			}
			if(ES2.Exists("lastPlayedWorld")) {
				currentWorld = ES2.Load<int>("lastPlayedWorld");
			}
			
			//enlik
			if(ES2.Exists("playerCoin")) {
				playerCoin = ES2.Load<int>("playerCoin");
			}
			if(ES2.Exists("unlockedChars")) {
				unlockedChars = ES2.LoadList<string>("unlockedChars");
			}
			if(ES2.Exists("boughtChars")) {
				boughtChars = ES2.LoadList<string>("boughtChars");
			}
			if(ES2.Exists("currentChar")) {
				currentChar = ES2.Load<string>("currentChar");
			}
			
			if(ES2.Exists("currentSkin")) {
				currentSkin = ES2.Load<int>("currentSkin");
			}
			if(ES2.Exists("socialRewards")) {
				socialRewards = ES2.LoadList<int>("socialRewards");
			}
			if(ES2.Exists("music")) {
				optionMusicIsOn = ES2.Load<bool>("music");
			}
			if(ES2.Exists("sound")) {
				optionSoundIsOn = ES2.Load<bool>("sound");
			}
			if(ES2.Exists("isLoginKakao")) {
				isLoginKakao = ES2.Load<bool>("isLoginKakao");
			}
			if(ES2.Exists("kakaoUserId")) {
				kakaoUserId = ES2.Load<string>("kakaoUserId");
			}
			if(ES2.Exists("kakaoUserName")) {
				kakaoUserName = ES2.Load<string>("kakaoUserName");
			}
			if(ES2.Exists("achievementRewards")) {
				achievementRewards = ES2.LoadList<int>("achievementRewards");
			}
			if(ES2.Exists("totalDeaths")) {
				totalDeaths = ES2.Load<int>("totalDeaths");
			}
			
		}
		catch (ES2Exception ex) {
			Debug.LogError(ex.Message);
			return;
		}
	}
}
