using UnityEngine;
using System.Collections;

public class DestroyDuplicates : MonoBehaviour {
	
	void OnLevelWasLoaded(int level){
		
		GameObject[] objectsInScene = FindObjectsOfType(typeof(GameObject)) as GameObject[];
		
		foreach(GameObject objectInScene in objectsInScene){
			
			if (gameObject.name == objectInScene.name + "(Clone)" && gameObject != objectInScene){
				DestroyImmediate(objectInScene);
				return;
			}
		}
	}
}
