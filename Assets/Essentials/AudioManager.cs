using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	GameData gameData;
	
	
	void Start (){
		gameData = GameObject.FindGameObjectWithTag("Game Data").GetComponent<GameData>();
		SelectMusic();
	}
	
	void OnLevelWasLoaded () {
		SelectMusic();
	}
	
	public void SelectMusic () {
		
		Debug.Log(Application.loadedLevelName);
		
		switch(Application.loadedLevelName) {
			case "EndlessPanda":
				CheckAndPlayMusic("Music_Endless");
				break;
		}
	}
	
	void CheckAndPlayMusic (string music) {
		if(!AudioController.IsPlaying(music)) {
			AudioController.PlayMusic(music);
		}
	}
}
