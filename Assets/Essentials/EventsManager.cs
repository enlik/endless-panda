using UnityEngine;
using System.Collections;

public class EventsManager : MonoBehaviour {
	
	//additional events
	public delegate void LogoutKakaoHandler ();
	
	public delegate void DataSaveDataHandler ();
	public delegate void DataLoadDataHandler ();
	
	public delegate void BounceCollectTargetHandler ();
	public delegate void BounceCollectCoinHandler ();//tambahan coin
	public delegate void BounceGameOverHandler (bool win);
	public delegate void BounceClearLevelHandler ();
	public delegate void DestroyDeathPoopHandler ();
	public delegate void BounceChangeBackgroundHandler (int num);
	public delegate void BouncePowerUpFreezeHandler (bool frozen);
	public delegate void BounceResetCoinHandler ();
	public delegate void BounceScreenHandler ();
	
	public delegate void LevelSelectChangeWorldHandler (int dir);
	public delegate void LevelSelectClearLevelButtonsHandler ();
	public delegate void LevelSelectUnlockWorldHandler ();
	
	public delegate void ShopSelectItemHandler (int id);
	
	public delegate void ToggleSkinHandler (int skin);
	
	public delegate void AnalyticsCheckDeathHandler (int level, int count);
	public delegate void AnalyticsCheckLevelQuitHandler (int level);
	public delegate void AnalyticsCheckCutsceneSkippedHandler ();
	public delegate void AnalyticsCheckCutsceneFinishedHandler ();
	
	public delegate void AnalyticsKakaoLoginPressedHandler ();
	public delegate void AnalyticsKakaoLogoutPressedHandler ();
	
	public delegate void AnalyticsKakaoLoginSucceedHandler ();
	public delegate void AnalyticsKakaoLogoutSucceedHandler ();
	public delegate void AnalyticsKakaoLoginFailedHandler ();
	public delegate void AnalyticsKakaoLogoutFailedHandler ();
	
	public delegate void AnalyticsCompleteLevelHandler (int level);
	
	//IAP
	public delegate void TransactionSuccessfulHandler (string productID, int productEnum);
	public delegate void TransactionFailedHandler (string message);
	
	//Social Networking
	public delegate void PostNotificationHandler(int socialType, bool status); //1 = fb, 2 = twitter
	
	
	public static event LogoutKakaoHandler onLogoutKakaoE;
	
	public static event DataSaveDataHandler onDataSaveDataE;
	public static event DataLoadDataHandler onDataLoadDataE;
	
	public static event BounceCollectTargetHandler onBounceCollectTargetE;
	
	public static event BounceCollectCoinHandler onBounceCollectCoinE; //tambahan coin
	
	public static event BounceGameOverHandler onBounceGameOverE;
	public static event BounceClearLevelHandler onBounceClearLevelE;
	public static event DestroyDeathPoopHandler onDestroyDeathPoopE;
	public static event BounceChangeBackgroundHandler onBounceChangeBackgroundE;
	public static event BouncePowerUpFreezeHandler onBouncePowerUpFreezeE;
	public static event BounceResetCoinHandler onBounceResetCoinE;
	public static event BounceScreenHandler onBounceShowFailResultSceneE;
	public static event BounceScreenHandler onBounceDisablePauseButtonE;
	
	public static event LevelSelectChangeWorldHandler onLevelSelectChangeWorldE;
	public static event LevelSelectClearLevelButtonsHandler onLevelSelectClearLevelButtonsE;
	public static event LevelSelectUnlockWorldHandler onLevelSelectUnlockWorldE;
	
	public static event ShopSelectItemHandler onShopSelectItemE;
	
	public static event ToggleSkinHandler onToggleSkinE;
	
	public static event AnalyticsCheckDeathHandler onAnalyticsCheckDeathE;
	public static event AnalyticsCheckLevelQuitHandler onAnalyticsCheckLevelQuitE;
	public static event AnalyticsCheckCutsceneSkippedHandler onAnalyticsCheckCutsceneSkippedE;
	public static event AnalyticsCheckCutsceneFinishedHandler onAnalyticsCheckCutsceneFinishedE;
	
	public static event AnalyticsKakaoLoginPressedHandler onAnalyticsKakaoLoginPressedE;
	public static event AnalyticsKakaoLogoutPressedHandler onAnalyticsKakaoLogoutPressedE;
	
	public static event AnalyticsKakaoLoginSucceedHandler onAnalyticsKakaoLoginSucceedE;
	public static event AnalyticsKakaoLogoutSucceedHandler onAnalyticsKakaoLogoutSucceedE;
	public static event AnalyticsKakaoLoginFailedHandler onAnalyticsKakaoLoginFailedE;
	public static event AnalyticsKakaoLogoutFailedHandler onAnalyticsKakaoLogoutFailedE; 
	
	public static event AnalyticsCompleteLevelHandler onAnalyticsCompleteLevelE;
	
	
	
	//IAP
	public static event TransactionSuccessfulHandler onTransactionSuccessfulE;
	public static event TransactionFailedHandler onTransactionFailedE;
	
	//Social Networking
	public static event PostNotificationHandler onPostNotificationE;
	
	
	public static void LogoutKakao () {
		if(onLogoutKakaoE != null){
			onLogoutKakaoE();
		}
	}
	
	public static void DataSaveData () {
		if(onDataSaveDataE != null) {
			onDataSaveDataE();
		}
	}
	
	public static void DataLoadData () {
		if(onDataLoadDataE != null) {
			onDataLoadDataE();
		}
	}
	
	public static void BounceCollectTarget () {
		if(onBounceCollectTargetE != null) {
			onBounceCollectTargetE();
		}
	}
	
	public static void BounceCollectCoin () { //koleksi koin
		if(onBounceCollectCoinE != null) {
			onBounceCollectCoinE();
		}
	}
	
	public static void BounceGameOver (bool win) {
		if(onBounceGameOverE != null) {
			onBounceGameOverE(win);
		}
	}
	
	public static void BounceClearLevel () {
		if(onBounceClearLevelE != null) {
			onBounceClearLevelE();
		}
	}
	
	public static void DestroyDeathPoop () {
		if(onDestroyDeathPoopE != null) {
			onDestroyDeathPoopE();
		}
	}
	
	public static void BounceChangeBackground (int num) {
		if(onBounceChangeBackgroundE != null) {
			onBounceChangeBackgroundE(num);
		}
	}
	
	public static void BouncePowerUpFreeze (bool frozen) {
		if(onBouncePowerUpFreezeE != null) {
			onBouncePowerUpFreezeE(frozen);
		}
	}
	
	public static void BounceResetCoin () {
		if (onBounceResetCoinE != null) {
			onBounceResetCoinE();
		}
	}
	
	public static void BounceShowFailResultScreen () {
		if (onBounceShowFailResultSceneE != null)
			onBounceShowFailResultSceneE();
	}
	
	public static void BounceDisablePauseButton () {
		if (onBounceDisablePauseButtonE != null)
			onBounceDisablePauseButtonE();
	}
	
	public static void LevelSelectChangeWorld (int dir) {
		if(onLevelSelectChangeWorldE != null) {
			onLevelSelectChangeWorldE(dir);
		}
	}
	
	public static void LevelSelectClearLevelButons () {
		if(onLevelSelectClearLevelButtonsE != null) {
			onLevelSelectClearLevelButtonsE();
		}
	}
	
	public static void LevelSelectUnlockWorld () {
		if(onLevelSelectUnlockWorldE != null) {
			onLevelSelectUnlockWorldE();
		}
	}
	
	public static void ShopSelectItem (int id) {
		if(onShopSelectItemE != null) {
			onShopSelectItemE(id);
		}
	}
	
	public static void ToggleSkin (int skin) {
		if(onToggleSkinE != null) {
			onToggleSkinE(skin);
		}
	}
	
	public static void AnalyticsCheckDeath (int level, int count) {
		if(onAnalyticsCheckDeathE != null) {
			onAnalyticsCheckDeathE(level, count);
		}
	}
	
	public static void AnalyticsCheckLevelQuit (int level) {
		if(onAnalyticsCheckLevelQuitE != null) {
			onAnalyticsCheckLevelQuitE(level);
		}
	}
	
	public static void AnalyticsCheckCutsceneSkipped () {
		if(onAnalyticsCheckCutsceneSkippedE != null) {
			onAnalyticsCheckCutsceneSkippedE();
		}
	}
	
	public static void AnalyticsCheckCutsceneFinished () {
		if(onAnalyticsCheckCutsceneFinishedE != null) {
			onAnalyticsCheckCutsceneFinishedE();
		}
	}
	
	
	public static void AnalyticsKakaoLoginPressed () {
		if(onAnalyticsKakaoLoginPressedE != null) {
			onAnalyticsKakaoLoginPressedE();
		}
	}
	
	public static void AnalyticsKakaoLogoutPressed () {
		if(onAnalyticsKakaoLogoutPressedE != null) {
			onAnalyticsKakaoLogoutPressedE();
		}
	}
	
	public static void AnalyticsKakaoLoginSucceed (){
		if(onAnalyticsKakaoLoginSucceedE != null){
			onAnalyticsKakaoLoginSucceedE();
		}
	}
	
	public static void AnalyticsKakaoLogoutSucceed (){
		if(onAnalyticsKakaoLogoutSucceedE != null){
			onAnalyticsKakaoLogoutSucceedE();
		}
	}
	
	public static void AnalyticsKakaoLoginFailed (){
		if(onAnalyticsKakaoLoginFailedE != null){
			onAnalyticsKakaoLoginFailedE();
		}
	}
	
	public static void AnalyticsKakaoLogoutFailed (){
		if(onAnalyticsKakaoLogoutFailedE != null){
			onAnalyticsKakaoLogoutFailedE();
		}
	}
	
	public static void AnalyticsCompleteLevel (int level){
		if(onAnalyticsCompleteLevelE != null){
			onAnalyticsCompleteLevelE(level);
		}
	}
	
	
	
	
	
	//IAP
	public static void IAPSuccessful (string productID, int productEnum) {
		if(onTransactionSuccessfulE != null) {
			onTransactionSuccessfulE(productID, productEnum);
		}
	}
	
	public static void IAPFail (string message) {
		if(onTransactionFailedE != null) {
			onTransactionFailedE(message);
		}
	}
	
	//Social Networking
	public static void PostNotification(int socialType, bool status){
		if(onPostNotificationE != null){
			onPostNotificationE(socialType, status);
		}
	}
}
