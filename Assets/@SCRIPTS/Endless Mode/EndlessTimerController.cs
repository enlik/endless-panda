using UnityEngine;
using System.Collections;
using System;

public class EndlessTimerController : MonoBehaviour {
	
	private static EndlessTimerController instance;
	public static EndlessTimerController Instance {
		get {
			return instance;
		}
	}
	
	public tk2dTextMesh txtTimer;
	public bool isTicking;
	public TimeSpan timeSpan;
	
	private int timeMinutes;
	private float timeSeconds;
	
	public int startMinutes;
	public float startSeconds;
	
	private void Awake () {
		instance = this;
	}
	
	private void Start () {
		timeMinutes = startMinutes;
		timeSeconds = startSeconds;
		UpdateTimeText();
	}
	
	void OnEnable () {
		EventsManager.onBounceClearLevelE += StopTimer;
	}
	
	void OnDisable () {
		StopTimer();
		EventsManager.onBounceClearLevelE -= StopTimer;
	}
	
	
	public void StopTimer () {
		StopCoroutine("CountdownTime");
		txtTimer.text = "00:00";
		txtTimer.Commit();
	}
	
	public void StartTimer () {
		timeMinutes = startMinutes;
		timeSeconds = startSeconds;
		StartCoroutine("CountdownTime");
	}

	private void UpdateTimeText () {
		if (timeSeconds < 10) {
			txtTimer.text = "0" + timeMinutes.ToString() + ":0" + Mathf.RoundToInt(timeSeconds).ToString();
			txtTimer.Commit();
		}
		
		else {
			txtTimer.text = "0" + timeMinutes.ToString() + ":" + Mathf.RoundToInt(timeSeconds).ToString();
			txtTimer.Commit();	
		}
		
	}
	
	
	private IEnumerator CountdownTime () {
		isTicking = true;
		while (isTicking) {
			while (timeMinutes >= 0 && timeSeconds >= 0){
				UpdateTimeText();
				yield return new WaitForSeconds(1);
				
				if(timeSeconds > 0){
					timeSeconds -= 1;
				}
				
				else {
					timeSeconds = 59;
					timeMinutes--;
				}
			}
			isTicking = false;
			print ("time's up!");
			EventsManager.BounceClearLevel();
			EndlessGameController.Instance.ShowResultScreen();
		}
	}
}
