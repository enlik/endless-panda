using UnityEngine;
using System.Text;
using System.Collections;
using System.Security.Cryptography;
using System.Collections.Generic;

public class EndlessGameController : MonoBehaviour {
	
	private static EndlessGameController instance;
	public static EndlessGameController Instance {
		get {
			return instance;
		}
	}
		
	
	#region Event
	public delegate void UIScoreHandler (int score);
	public static event UIScoreHandler onUpdateUIScoreEvent;
	public static event UIScoreHandler onUpdateUIHighScoreEvent;
	
	private static void UpdateUIscore (int score) {
		if (onUpdateUIScoreEvent != null)
			onUpdateUIScoreEvent(score);
	}
	#endregion
	
	
	// chocolate
	private const string bounceTargetPath = "Bounce/BounceTarget";
	private const string platformPath = "Bounce/BouncePlatform_1";
	private Transform chocolate;
	private int lastChocolateIndex = 0;
	public List<Transform> targetPos;
	
	// player
	private const string playerPath = "Bounce/BouncePlayer";
	private BouncePlayerController player;
	public Transform playerInitPos;
	
	// enemy
	public List<BounceItemController> enemies;
	public List<BounceItemController> platforms;
	private List<Vector3> enemiesInitPos = new List<Vector3>();
	
	// Score
	private int score = 0;
	
	public tk2dTextMesh txtScore;
	public tk2dTextMesh txtTimer;
	private float timerValue;
	
	private System.DateTime timerValueMax;
	
	public GameObject objTutorialBox;
	public GameObject objResultScreenBox;
	public tk2dUIItem objButtonNext;
	public tk2dUIItem objButtonFinish;
	public tk2dTextMesh txtResultScore;
	
	public float timeSecondsTutorial;
	public tk2dTextMesh txtTouchToSkip;
	
	
	public bool isFinish;
	
	public int Score {
		set {
			score = value;
			UpdateUIscore(score);
		}
		get {
			return score;
		}
	}

	
	
	#region Mono
	private void Awake () {
		instance = this;
		InitEnemy();
	}
	
	private void Start () {
		StartCoroutine("CountdownTimeTutorial");
		objTutorialBox.SetActive(true);
		objResultScreenBox.SetActive(false);
		DisableEnemy();
		isFinish = false;
	}
	
	private void OnEnable () {
		EventsManager.onBounceCollectTargetE += PlayerCollectTarget;
		EventsManager.onBounceDisablePauseButtonE += DisablePauseButton;
		EventsManager.onBounceShowFailResultSceneE += ShowResultScreen;
		objButtonNext.OnClick += OnButtonNext;
		objButtonFinish.OnClick += OnButtonFinish;
	}

	
	private void OnDisable () {
		EventsManager.onBounceCollectTargetE -= PlayerCollectTarget;
		EventsManager.onBounceDisablePauseButtonE -= DisablePauseButton;
		EventsManager.onBounceShowFailResultSceneE -= ShowResultScreen;
		objButtonNext.OnClick -= OnButtonNext;
		objButtonFinish.OnClick -= OnButtonFinish;
	}
	#endregion
	
	#region Event
	private void PlayerCollectTarget () {
		RandomSpawnTarget();
		AudioController.Play("Sound_Get");
		
		//Maximum score is 10k, Player can't reach more than 10k scores
		if (Score < 10000){
			Score += 250;	
		}
		txtScore.text = Score.ToString();
		Debug.Log(score);
	}
	
	private void DisablePauseButton () {
		
	}
	
	public void ShowResultScreen () {
		isFinish = true;
		if(isFinish){
			objResultScreenBox.SetActive(true);
			txtResultScore.text = "BONUS POIN:\n" + Score.ToString();
			txtResultScore.Commit();
			DisableEnemy();
			isFinish = false;
		}
	}
	
	#endregion
	
	
	#region Core
	public void StartLevel () 
	{	
		EndlessTimerController.Instance.StartTimer();
		txtScore.text = "0";
		
		if (player != null) Destroy(player.gameObject);
		if (chocolate != null) Destroy(chocolate.gameObject);
		
		
		SpawnPlayer();
		RandomSpawnTarget();
		ReInitEnemy();
		
		Score = 0;
		
		EndOfStartTransition();
	}
	
	private void EndOfStartTransition () {
		player.isActive = true;
		EnableEnemy();
	}
	
	public void EndLevel () {
		objTutorialBox.SetActive(true);
		timeSecondsTutorial = 15f;
		StartCoroutine("CountdownTimeTutorial");
//		DisableEnemy();
	}
	#endregion
	
	
	#region Chocolate Spawner
	private void RandomSpawnTarget () {
		int locationIndex;
		
		do {
			locationIndex = Random.Range(0, targetPos.Count);
		}
		while (locationIndex == lastChocolateIndex);
		
		SpawnChocolate(targetPos[locationIndex].position);
		
		lastChocolateIndex = locationIndex;
	}
	
	private void SpawnChocolate (Vector3 position) {
		chocolate = Instantiate(Resources.Load(bounceTargetPath, typeof(Transform))) as Transform;
		chocolate.position = position;
		chocolate.gameObject.SetActive(true);
	}
	
	private void RecycleChocolate () {
		chocolate.gameObject.SetActive(false);
	}
	#endregion
	
	
	
	#region Player Spawner
	private void SpawnPlayer () {
		GameObject objPlayer = Instantiate(Resources.Load("Bounce/BouncePlayer", typeof(GameObject))) as GameObject;
		player = objPlayer.GetComponent<BouncePlayerController>();
		player.transform.position = playerInitPos.position;
		player.gameObject.SetActive(true);
	}
	
	private void RecyclePlayer () {
		player.gameObject.SetActive(false);
	}
	#endregion
	
	
	#region Enemy
	public void InitEnemy () {
		foreach (BounceItemController enemy in enemies) {
			enemiesInitPos.Add(enemy.transform.position);
		}
	}
	
	private void ReInitEnemy () {
		for (int i = 0; i < enemies.Count; i++) {
			enemies[i].transform.position = enemiesInitPos[i];
		}
	}
	
	private void EnableEnemy () {
		foreach (BounceItemController enemy in enemies) {
			enemy.isFrozen = false;
		}
		
		foreach (BounceItemController platform in platforms) {
			platform.isFrozen = false;
		}
	}
	
	private void DisableEnemy () {
		foreach (BounceItemController enemy in enemies) {
			enemy.isFrozen = true;
		}
		
		foreach (BounceItemController platform in platforms) {
			platform.isFrozen = true;
		}
		
	}
	#endregion
	
	#region Button Method
	void OnButtonNext () {
		print("OnButtonNext");
		objTutorialBox.SetActive(false);
		StartLevel();
		StopCoroutine("CountdownTimeTutorial");
	}
	
	void OnButtonFinish () {
		print("OnButtonFinish");
		objResultScreenBox.SetActive(false);
		EndLevel();
	}
	
	#endregion
	
	private IEnumerator CountdownTimeTutorial () {
		while (timeSecondsTutorial > 0){
			txtTouchToSkip.text = timeSecondsTutorial + "s";
			yield return new WaitForSeconds(1);
			timeSecondsTutorial--;
		}
		OnButtonNext();
	}
	
	
	
	

	
	
	
	
	
	
}
