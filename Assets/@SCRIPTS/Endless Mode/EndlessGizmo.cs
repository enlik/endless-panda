using UnityEngine;
using System.Collections;

public class EndlessGizmo : MonoBehaviour {
	
	public float radius = 0.5f;
	public Color color;
	
	void OnDrawGizmos () {
		Gizmos.color = color;
		Gizmos.DrawSphere(transform.position, radius);
	}
}
