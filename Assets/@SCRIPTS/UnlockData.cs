using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnlockData : MonoBehaviour {

	public Dictionary<int,string> levelRequirements;
	
	public int levelReq1;
	public int levelReq2;
	
	void Awake () {
		InitializeLevelRequirements();
	}
	
	void InitializeLevelRequirements () {
		levelRequirements = new Dictionary<int,string>();
		levelRequirements.Add(1, " 0, 0");
		levelRequirements.Add(2, " 1, 0");
		levelRequirements.Add(3, " 1, 0");
		levelRequirements.Add(4, " 2, 0");
		levelRequirements.Add(5, " 2, 3");
		levelRequirements.Add(6, " 3, 0");
		levelRequirements.Add(7, " 4, 0");
		levelRequirements.Add(8, " 4, 5");
		levelRequirements.Add(9, " 5, 6");
		levelRequirements.Add(10, " 6, 0");
		levelRequirements.Add(11, " 7, 0");
		levelRequirements.Add(12, " 7, 8");
		levelRequirements.Add(13, " 8, 9");
		levelRequirements.Add(14, " 9,10");
		levelRequirements.Add(15, "10, 0");
		levelRequirements.Add(16, "11, 0");
		levelRequirements.Add(17, "11,12");
		levelRequirements.Add(18, "12,13");
		levelRequirements.Add(19, "13,14");
		levelRequirements.Add(20, "14,15");
		levelRequirements.Add(21, "16,17");
		levelRequirements.Add(22, "17,18");
		levelRequirements.Add(23, "18,19");
		levelRequirements.Add(24, "19,20");
		levelRequirements.Add(25, "21,22");
		levelRequirements.Add(26, "22,23");
		levelRequirements.Add(27, "23,24");
		levelRequirements.Add(28, "25,26");
		levelRequirements.Add(29, "26,27");
		levelRequirements.Add(30, "28,29");
		levelRequirements.Add(31, "30, 0");
		levelRequirements.Add(32, "31, 0");
		levelRequirements.Add(33, "31, 0");
		levelRequirements.Add(34, "32, 0");
		levelRequirements.Add(35, "32,33");
		levelRequirements.Add(36, "33, 0");
		levelRequirements.Add(37, "34, 0");
		levelRequirements.Add(38, "34,35");
		levelRequirements.Add(39, "35,36");
		levelRequirements.Add(40, "36, 0");
		levelRequirements.Add(41, "37, 0");
		levelRequirements.Add(42, "37,38");
		levelRequirements.Add(43, "38,39");
		levelRequirements.Add(44, "39,40");
		levelRequirements.Add(45, "40, 0");
		levelRequirements.Add(46, "41, 0");
		levelRequirements.Add(47, "41,42");
		levelRequirements.Add(48, "42,43");
		levelRequirements.Add(49, "43,44");
		levelRequirements.Add(50, "44,45");
		levelRequirements.Add(51, "46,47");
		levelRequirements.Add(52, "47,48");
		levelRequirements.Add(53, "48,49");
		levelRequirements.Add(54, "49,50");
		levelRequirements.Add(55, "51,52");
		levelRequirements.Add(56, "52,53");
		levelRequirements.Add(57, "53,54");
		levelRequirements.Add(58, "55,56");
		levelRequirements.Add(59, "56,57");
		levelRequirements.Add(60, "58,59");
		levelRequirements.Add(61, "60, 0");
		levelRequirements.Add(62, "61, 0");
		levelRequirements.Add(63, "61, 0");
		levelRequirements.Add(64, "62, 0");
		levelRequirements.Add(65, "62,63");
		levelRequirements.Add(66, "63, 0");
		levelRequirements.Add(67, "64, 0");
		levelRequirements.Add(68, "64,65");
		levelRequirements.Add(69, "65,66");
		levelRequirements.Add(70, "66, 0");
		levelRequirements.Add(71, "67, 0");
		levelRequirements.Add(72, "67,68");
		levelRequirements.Add(73, "68,69");
		levelRequirements.Add(74, "69,70");
		levelRequirements.Add(75, "70, 0");
		levelRequirements.Add(76, "71, 0");
		levelRequirements.Add(77, "71,72");
		levelRequirements.Add(78, "72,73");
		levelRequirements.Add(79, "73,74");
		levelRequirements.Add(80, "74,75");
		levelRequirements.Add(81, "76,77");
		levelRequirements.Add(82, "77,78");
		levelRequirements.Add(83, "78,79");
		levelRequirements.Add(84, "79,80");
		levelRequirements.Add(85, "81,82");
		levelRequirements.Add(86, "82,83");
		levelRequirements.Add(87, "83,84");
		levelRequirements.Add(88, "85,86");
		levelRequirements.Add(89, "86,87");
		levelRequirements.Add(90, "88,89");
	}
	
	void GetRequirements (int key) {
		string str = "";
		levelRequirements.TryGetValue(key, out str);
		int.TryParse((str.Remove(2,3)), out levelReq1);
		int.TryParse((str.Remove(0,3)), out levelReq2);
	}
	
	public void UnlockLevels () {
		for(int i = 0; i < 90; i++) {
			GetRequirements(i+1);
			if(this.GetComponent<GameData>().completedLevels.Contains(levelReq1) &&
				this.GetComponent<GameData>().completedLevels.Contains(levelReq2)){
				if(!this.GetComponent<GameData>().unlockedLevels.Contains(i+1)) {
					this.GetComponent<GameData>().unlockedLevels.Add(i+1);
				}
			}
		}
		this.GetComponent<GameData>().unlockedLevels.Sort();
	}
}
