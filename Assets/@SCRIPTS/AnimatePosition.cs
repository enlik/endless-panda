using UnityEngine;
using System.Collections;

public class AnimatePosition : MonoBehaviour {

	public bool animateAtStart;
	public bool isPingPong;
	
	public Vector3 offset;
	public float time;
	
	void Start () {
		if(animateAtStart) {
			TweenPosition();
		}
	}
	
	void TweenPosition () {
		if(isPingPong) {
			iTween.MoveBy(this.gameObject, iTween.Hash("x", offset.x, "y", offset.y, "z", offset.z, "time", time, "easetype", "linear",
				"looptype", iTween.LoopType.pingPong));
		}
		else {
			iTween.MoveBy(this.gameObject, iTween.Hash("x", offset.x, "y", offset.y, "z", offset.z, "time", time, "easetype", "linear"));
		}
	}
}
