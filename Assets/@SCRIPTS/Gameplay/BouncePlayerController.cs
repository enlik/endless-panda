using UnityEngine;
using System.Collections;

public class BouncePlayerController : MonoBehaviour {
	
	private static BouncePlayerController instance;
	public static BouncePlayerController Instance {
		get {
			return instance;
		}
	}
	
	public GameData gameData;
	public GameObject theCamera;
	public GameObject sprite;
//	public GameObject crack;


	public Transform transformChar;
	public float initialCharScaleX;
	public float initialCharScaleY;
	public float initialCharScaleZ;
	
	public bool isActive;
	
	public float accelerationX;
	public float deccelerationX;
	public float neutralThreshold;
	
	public float platformBounceSpeed;
	public float trampolineBounceSpeed;
	
	public int lastInputDirection;
	public float secondChanceDirection;
	
	public float moveX;
	public float moveY;
	
	public float maxX;
	public float minY;
	
	public float gravity;
	
	public float deathAnimationDuration;
	public Vector3[,] deathAnimationVectors;
	
	public bool fallSoundPlayed;
	
	private tk2dSpriteCollectionData sprCollData;
	private bool isHaveSecondLife;
	
	public bool isInvincible;
	
	
	void Awake () {
		gameData = GameObject.FindGameObjectWithTag("Game Data").GetComponent<GameData>();
		theCamera = GameObject.FindGameObjectWithTag("MainCamera");
		sprite = transform.Find("Sprite").gameObject;
		sprCollData = sprite.GetComponent<tk2dSprite>().Collection;
		
	}
	
	void OnEnable () {
		EventsManager.onBounceClearLevelE += OnBounceClearLevel;
		
		Gesture.onTouchE += OnTouch;
		Gesture.onMouse1E += OnMouseOne;
	}
	
	void OnDisable () {
		EventsManager.onBounceClearLevelE -= OnBounceClearLevel;
		
		Gesture.onTouchE -= OnTouch;
		Gesture.onMouse1E -= OnMouseOne;
	}
	
	void Start () {
		sprite.GetComponent<tk2dSprite>().SetSprite("panda_1_0");
//		isActive = false;
		isInvincible = false;
		initialCharScaleX = transformChar.localScale.x;
		initialCharScaleY = transformChar.localScale.y;
		initialCharScaleZ = transformChar.localScale.z;
		isHaveSecondLife = false;
		InitiateDeathVectors();
		
	}
	
	void Update () {
		
		if(isActive && Time.timeScale != 0f) {
			if(Input.touchCount > 0) {
				if(Input.touches[Input.touchCount-1].position.x < Screen.width*0.5f) {
					if(lastInputDirection != -1) {
						moveX = 0;
						lastInputDirection = -1;
					}
				}
				else if(Input.touches[Input.touchCount-1].position.x > Screen.width*0.5f) {
					if(lastInputDirection != 1) {
						moveX = 0;
						lastInputDirection = 1;
					}
				}
			}
			#if UNITY_EDITOR
			else if(Input.GetKey(KeyCode.LeftArrow)) {
				if(lastInputDirection != -1) {
					moveX = 0;
					lastInputDirection = -1;
				}
			}
			else if(Input.GetKey(KeyCode.RightArrow)) {
				if(lastInputDirection != 1) {
					moveX = 0;
					lastInputDirection = 1;
				}
			}
			else if(Input.GetKey(KeyCode.A)) {
				if(lastInputDirection != -1) {
					moveX = 0;
					lastInputDirection = -1;
				}
			}
			else if(Input.GetKey(KeyCode.D)) {
				if(lastInputDirection != 1) {
					moveX = 0;
					lastInputDirection = 1;
				}
			}
			#endif
			
			
			#if UNITY_EDITOR
			if(Input.GetKey(KeyCode.LeftArrow) && !isInvincible) {
				AcceleratePlayer(lastInputDirection);
			}
			else if(Input.GetKey(KeyCode.RightArrow) && !isInvincible) {
				AcceleratePlayer(lastInputDirection);
			}
			else if(Input.GetKey(KeyCode.A) && !isInvincible) {
				AcceleratePlayer(lastInputDirection);
			}
			else if(Input.GetKey(KeyCode.D) && !isInvincible) {
				AcceleratePlayer(lastInputDirection);
			}
			#endif
			else {
				DecceleratePlayer();
			}
			
		}
	}
	
	void FixedUpdate () {
		if(isActive) {
			transform.Translate(new Vector3(moveX, moveY, 0f));
			ApplyGravity();
			AnimateSprite();
			if(transform.position.y < -120f) {
				isActive = false;
				PlayerDeath(1);
			}
		}
	}
	
	void InitiateDeathVectors () {
		deathAnimationVectors = new Vector3[10,5];
		for(int i = 0; i < 10; i++) {
			int j = 0;
			deathAnimationVectors[i,j] = new Vector3((i*50f)-200f, 75f, -3f);
			j++;
			deathAnimationVectors[i,j] = new Vector3((i*50f)-225f, 40f, -3f);
			j++;
			deathAnimationVectors[i,j] = new Vector3((i*50f)-200f, 0f, -3f);
			j++;
			deathAnimationVectors[i,j] = new Vector3((i*50f)-225f, -40f, -3f);
			j++;
			deathAnimationVectors[i,j] = new Vector3((i*50f)-200f, -75f, -3f);
		}
	}

	
//	
//	void CameraFollow () {
//		theCamera.transform.position = new Vector3(transform.position.x, theCamera.transform.position.y, theCamera.transform.position.z);
//	}
	
		
	void AnimateSprite () {
		if(isActive && !isInvincible) {	
				sprite.GetComponent<tk2dSprite>().spriteId = sprCollData.GetSpriteIdByName("panda_1_0");
				
				//atas
				if(moveY < 1.5f && moveY > -1.5f) {
					if (lastInputDirection == -1){
						transformChar.localScale = new Vector3(-1f, 1f, 1f);
					}
					
					else{
						transformChar.localScale = new Vector3(1f, 1f, 1f);
					}
					
				}
				//bawah gepeng
				else if(moveY > 3f) {
					if (lastInputDirection == -1){
						transformChar.localScale = new Vector3(-1.2f, 0.7f, 1f);
					}
					
					else{
						transformChar.localScale = new Vector3(1.2f, 0.7f, 1f);
					}
					
				}
				//atas
				else if(moveY < 0f) {
					if (lastInputDirection == -1){
						transformChar.localScale = new Vector3(-1f, 1f, 1f);
					}
					else{
						transformChar.localScale = new Vector3(1f, 1f, 1f);
					}
					
				}
				
				//mau naik
				else if(moveY > 0f) {
					if (lastInputDirection == -1){
						transformChar.localScale = new Vector3(-0.9f, 1f, 1f);
					}
					else{
						transformChar.localScale = new Vector3(0.9f, 1f, 1f);
					}
				}	
			}	
		
	}
	
	void OnTouch (Vector2 touch) {
		if (!isInvincible){
			if(touch.x < Screen.width*0.5f) {
				AcceleratePlayer(-1);
			}
			else {
				AcceleratePlayer(1);
			}
		}
	}
	
	void OnMouseOne (Vector2 mouse) {
		if (!isInvincible){
			if(mouse.x < Screen.width*0.5f) {
				AcceleratePlayer(-1);
			}
			else {
				AcceleratePlayer(1);
			}	
		}
	}
	
	void OnBounceClearLevel () {
		Destroy(this.gameObject);
	}
	
	void AcceleratePlayer (int direction) {
		if(isActive && !isInvincible && Time.timeScale != 0) {
			if(direction < 0) transform.localScale = new Vector3(-1f, 1f, 1f);
			else transform.localScale = new Vector3(1f, 1f, 1f);
			if(moveX < maxX && moveX > -maxX) {
				moveX += direction * accelerationX;
			}
		}
	}
	
	
	void DecceleratePlayer () {
		if(isActive) {
			if(moveX < 0f) {
				moveX += deccelerationX;
			}
			else if(moveX > 0f) {
				moveX -= deccelerationX;
			}
			if(moveX < neutralThreshold && moveX > -neutralThreshold) {
				moveX = 0f;
			}
		}
	}
	
	void ApplyGravity () {
		if(isActive) {
			if(moveY > minY) {
				moveY -= gravity * Time.deltaTime * EndlessPandaCommon.speedConstant;
			}
		}
	}
	
	void OnTriggerEnter (Collider col) {
		if(isActive) {
			if(col.gameObject.tag == "Bounce Platform") {
				if(transform.position.y > col.transform.position.y) {
					if(col.GetComponent<BounceItemController>().isTrampoline) {
						AudioController.Play("Sound_Bounce_Trampoline");
						moveY = trampolineBounceSpeed;
						col.GetComponent<BounceItemController>().trampoline.GetComponent<tk2dSpriteAnimator>().Play();
						//Debug.Log("bounce trampoline");
					}
					else {
						AudioController.Play("Sound_Bounce_Basic");
						moveY = platformBounceSpeed;
						//Debug.Log("bounce basic");
						
					}
					fallSoundPlayed = false;
				}
			}
			else if(col.gameObject.tag == "Bounce Target") {
				EventsManager.BounceCollectTarget();
				Destroy(col.gameObject);
			}
			else if(col.gameObject.tag == "Bounce Obstacle") {
				if (!isHaveSecondLife && !isInvincible && isActive) {	
					Debug.Log("game over!");
					//spawn coin damage
					isActive = false;
					if(col.transform.position.x > transform.position.x) {
						PlayerDeath(1);
					}
					else {
						PlayerDeath(-1);
					}
				}
				
				//check this
				else{
					if(!isInvincible && isActive){
						AudioController.Play("Sound_Hit");
						sprite.GetComponent<tk2dSprite>().SetSprite("panda_death");
					}
				}
			}
		}
	}
	
	
	
	
	void PlayerStepBack () {
		isActive = false;
		float stepSpeed = 100;
		float distance = 20;
		float step = 0;
		switch (lastInputDirection) {
		case 1 :
			step = transform.position.x - distance;
			break;
		case -1 :
			step = transform.position.x + distance;
			break;
		}
		
		moveX = 0;
		
		iTween.MoveTo(this.gameObject, 
			iTween.Hash(
			"position", new Vector3(step, 0f, 0f),
			"speed", stepSpeed, 
			"easetype", iTween.EaseType.easeOutExpo,
			"oncomplettarget", gameObject,
			"oncomplete", "EndOfStepBack")
			);
	}
	
	void EndOfStepBack () {
		isActive = true;
	}
	
	void OnInvincibleEnd () {
		Debug.Log("vulnerable");
		isInvincible = false;
		
	}
	
	
//	void PlayerDeath (int dir) {
//		EventsManager.BounceDisablePauseButton();
//		
//		isActive = false;
//		AudioController.Play("Sound_Death");
//		sprite.GetComponent<tk2dSprite>().spriteId = sprite.GetComponent<tk2dSprite>().Collection.GetSpriteIdByName("panda_death");
//		
//		//normalize sprite scale
//		transformChar.localScale = Vector3.one;
//		
////		iTween.ScaleTo(this.gameObject, iTween.Hash("scale", new Vector3(transform.localScale.x*1f, transform.localScale.y*1f,
////			1f), "time", deathAnimationDuration, "easetype", iTween.EaseType.easeOutExpo));
//		
//		//todo
//		iTween.MoveTo(this.gameObject, iTween.Hash("position", new Vector3(0f, 3.5f, transform.position.z-8f),
//			"time", deathAnimationDuration, "easetype", iTween.EaseType.easeOutExpo));
//		
//		StartCoroutine(ShowCrackGlass());
//	}
	
	void PlayerDeath (int dir) {
		
		isActive = false;
		
		AudioController.Play("Sound_Death");
		sprite.GetComponent<tk2dSprite>().spriteId = sprite.GetComponent<tk2dSprite>().Collection.GetSpriteIdByName("panda_death");
		
		//normalize localScale to prevent 'gepeng' size on PlayerDeath
		if (lastInputDirection == -1){
			transformChar.localScale = new Vector3(-1f, 1f, 1f);
		}
		else{
			transformChar.localScale = new Vector3(1f, 1f, 1f);
		}
		
		iTween.ScaleBy(this.gameObject, new Vector3(3f, 3f, 1f), deathAnimationDuration);
		iTween.MoveTo(this.gameObject, new Vector3(0f, 0f, -5f), deathAnimationDuration);
		iTween.RotateTo(this.gameObject, new Vector3(0f, 0f, Random.Range(30f, 60f) * dir), deathAnimationDuration);
//		transform.localScale = new Vector3(transform.localScale.x/3f, transform.localScale.y/3f, 1f);
//		iTween.ScaleTo(this.gameObject, iTween.Hash("scale", new Vector3(transform.localScale.x*5f, transform.localScale.y*5f,
//			1f), "time", deathAnimationDuration, "easetype", iTween.EaseType.easeOutExpo));
//		iTween.MoveTo(this.gameObject, iTween.Hash("position", new Vector3(transform.position.x+(-30f*dir), transform.position.y+30f, transform.position.z-5f),
//			"time", deathAnimationDuration, "easetype", iTween.EaseType.easeOutExpo));
//		iTween.RotateTo(this.gameObject, iTween.Hash("rotation", new Vector3(0f, 0f, transform.rotation.z + (Random.Range(30f, 60f) * dir)), "time", deathAnimationDuration,
//			"easetype", iTween.EaseType.easeOutExpo));
//		yield return new WaitForSeconds(0.1f);
//		GameObject deathPoo = (GameObject)Instantiate(Resources.Load("Bounce/DeathPoo"), new Vector3(transform.position.x,
//			transform.position.y, -5f), Quaternion.Euler(0f, 0f, 0f));
//		iTween.ScaleTo(deathPoo.gameObject, iTween.Hash("scale", new Vector3(200f, 200f, 0f), "time", deathAnimationDuration,
//			"easetype", iTween.EaseType.easeOutExpo));
//		int[] randomPoop = new int[70];
//		for(int i = 0; i < 70; i++) randomPoop[i] = i;
//		for(int i = 0; i < 70; i++) {
//			int temp = randomPoop[i];
//			int random = Random.Range(0,70);
//			randomPoop[i] = randomPoop[random];
//			randomPoop[random] = temp;
//		}
//		for(int i = 0; i < 10; i++) {
//			Instantiate(Resources.Load("Bounce/DeathPoop"), deathAnimationVectors[randomPoop[i]], Quaternion.Euler(0f, 0f, 0f));
//		}
		
		StartCoroutine(ShowDeathPoop());
		
		
	}
	
	IEnumerator ShowDeathPoop () {
		for(int j = 0; j < 5; j++) {
			for(int i = 0; i < 10; i++) {
				Instantiate(Resources.Load("Bounce/DeathPoop"), deathAnimationVectors[i,j], Quaternion.Euler(0f, 0f, 0f));
			}
			yield return new WaitForSeconds(0.05f);
		}
		yield return new WaitForSeconds(0.3f);
		EventsManager.BounceClearLevel();
		isActive = true;
		EndlessGameController.Instance.ShowResultScreen();
	}
}
