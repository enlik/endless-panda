﻿using UnityEngine;
using System.Collections;

public class BounceDeathPoopController : MonoBehaviour {
	
	private static BounceDeathPoopController instance;
	public static BounceDeathPoopController Instance {
		get {
			return instance;
		}
	}
	
	void OnEnable () {
		EventsManager.onBounceClearLevelE += OnBounceClearLevel;
		EventsManager.onDestroyDeathPoopE += DestroyDeathPoop;
	}
	
	void OnDisable () {
		EventsManager.onBounceClearLevelE -= OnBounceClearLevel;
		EventsManager.onDestroyDeathPoopE -= DestroyDeathPoop;
	}
	
	public void DestroyDeathPoop () {
		Destroy(this.gameObject);
	}
	
	public void OnBounceClearLevel () {
		Destroy(this.gameObject);
	}
}
