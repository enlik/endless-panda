using UnityEngine;
using System.Collections;

public class BounceItemController : MonoBehaviour {
	
	public GameObject sprite;
	public GameObject trampoline;
	public GameObject smoke;
	
	public GameObject bounceTarget;
	
	public int directionX;
	public int directionY;
	
	public float speedX;
	public float speedY;
	
	public float minX;
	public float maxX;
	public float minY;
	public float maxY;
	
	public bool isReverseX;
	public bool isReverseY;
	public Vector3 lastTurn;
	public float turnDistance;
	
	public bool isTrampoline;
	
	public int worldId;
	public float lastToggle;
//	public float disappearToggleTime;
//	public float disappearFadeTime;
	public float disappearOnDuration;
	public float disappearOffDuration;
	public bool delayedDisappear; //tambahan delay disappearing platform
	
	public bool isFrozen;
	
	void Awake () {
//		if(disappearToggleTime > 0f) {
//			sprite = transform.Find("Sprite").gameObject;
//		}
		if(disappearOnDuration > 0f && disappearOffDuration > 0f) {
			sprite = transform.Find("Sprite").gameObject;
		}
		if(isTrampoline) {
			trampoline = transform.Find("Trampoline").gameObject;
		}
	}
	
	void OnEnable () {
		EventsManager.onBounceClearLevelE += OnBounceClearLevel;
		EventsManager.onBouncePowerUpFreezeE += OnBouncePowerUpFreeze;
	}
	
	void OnDisable () {
		EventsManager.onBounceClearLevelE -= OnBounceClearLevel;
		EventsManager.onBouncePowerUpFreezeE -= OnBouncePowerUpFreeze;
	}
	
	void Start () {
//		if(speedX > 0) directionX = 1;
//		if(speedY > 0) directionY = 1;

		CheckDirections();

		if(delayedDisappear) {
			float temp = disappearOnDuration;
			disappearOnDuration = disappearOffDuration;
			disappearOffDuration = temp;
		}

		if(disappearOnDuration > 0f && disappearOffDuration > 0f) {
			StartDisappearTimer(); //tambahan delay
			//StartCoroutine(DisappearOnTimer());
		}
	}
	
	void FixedUpdate () {
		if(!isFrozen) {
			transform.Translate(new Vector3(speedX * directionX, speedY * directionY, 0f));
			CheckDirections();
		}
	}
	
	void OnBounceClearLevel () {
//		Destroy(this.gameObject);
		//do nothing
	}
	
	void OnBouncePowerUpFreeze (bool frozen) {
		if(isFrozen != frozen) {
			isFrozen = frozen;
		}
	}
	
	void CheckDirections () {
		if(directionX == 1 && transform.position.x >= maxX) {
			directionX = -1;
		}
		if(directionX == -1 && transform.position.x <= minX) {
			directionX = 1;
		}
		if(directionY == 1 && transform.position.y >= maxY) {
			directionY = -1;
		}
		if(directionY == -1 && transform.position.y <= minY) {
			directionY = 1;
		}
//		if(Vector3.Distance(lastTurn, transform.position) >= turnDistance) {
//			lastTurn = transform.position;
//			directionX *= -1;
//			directionY *= -1;
//		}
	}
	void StartDisappearTimer () {
		if(delayedDisappear) {
			collider.enabled = false;
			sprite.GetComponent<tk2dSpriteAnimator>().Play("platform_disappear_"+worldId+"_1");
			iTween.MoveBy(sprite.gameObject, new Vector3(0f, 0f, 10f), 1f);
			StartCoroutine(DisappearOffTimer());
		}
		else {
			StartCoroutine(DisappearOnTimer());
		}
	}
	
	IEnumerator DisappearOnTimer () {
		while(collider.enabled) {
			yield return new WaitForSeconds(0.1f);
			if(Time.time > lastToggle + disappearOnDuration) {
				lastToggle = Time.time;
				collider.enabled = false;
				sprite.GetComponent<tk2dSpriteAnimator>().Play("platform_disappear_"+worldId+"_1");
				iTween.MoveBy(sprite.gameObject, new Vector3(0f, 0f, 0f), 1f);
				StartCoroutine(DisappearOffTimer());
			}
		}
	}
	
	IEnumerator DisappearOffTimer () {
		while(!collider.enabled) {
			yield return new WaitForSeconds(0.1f);
			if(Time.time > lastToggle + disappearOffDuration) {
				lastToggle = Time.time;
				collider.enabled = true;
				sprite.GetComponent<tk2dSpriteAnimator>().Play("platform_disappear_"+worldId+"_2");
				iTween.MoveBy(sprite.gameObject, new Vector3(0f, 0f, 0f), 1f);
				StartCoroutine(DisappearOnTimer());
			}
		}
	}
	
//	IEnumerator DisappearTimer () {
//		while(true) {
//			yield return new WaitForSeconds(0.1f);
//			if(Time.time > lastToggle + disappearToggleTime) {
//				lastToggle = Time.time;
//				ToggleDisappear();
//			}
//		}
//	}
//	
//	void ToggleDisappear () {
//		if(collider.enabled == true) {
//			collider.enabled = false;
//			sprite.GetComponent<tk2dAnimatedSprite>().Play("platform_disappear_2_1");
////			iTween.ValueTo(sprite.gameObject, iTween.Hash("from", 1f, "to", 0f, "time", disappearFadeTime, "onupdate", "TweenDisappear",
////				"onupdatetarget", this.gameObject, "oncomplete", "TweenDisappearComplete", "oncompleteparams", false,
////				"oncompletetarget", this.gameObject));
//		}
//		else {
//			collider.enabled = true;
//			sprite.GetComponent<tk2dAnimatedSprite>().Play("platform_disappear_2_2");
////			iTween.ValueTo(sprite.gameObject, iTween.Hash("from", 0f, "to", 1f, "time", disappearFadeTime, "onupdate", "TweenDisappear",
////				"onupdatetarget", this.gameObject, "oncomplete", "TweenDisappearComplete", "oncompleteparams", true,
////				"oncompletetarget", this.gameObject));
//		}
//	}
//	
//	void TweenDisappear (float newValue) {
//		sprite.GetComponent<tk2dSprite>().color = new Color(sprite.GetComponent<tk2dSprite>().color.r,
//			sprite.GetComponent<tk2dSprite>().color.g, sprite.GetComponent<tk2dSprite>().color.b, newValue);
//	}
//	
//	void TweenDisappearComplete (bool toggle) {
//		if(!toggle) {
//			collider.enabled = toggle;
//		}
//	}
}
