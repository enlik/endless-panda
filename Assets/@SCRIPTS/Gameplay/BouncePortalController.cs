using UnityEngine;
using System.Collections;

public class BouncePortalController : MonoBehaviour {

	public GameObject objPortalPair;
	public GameObject spiral;
	public bool isActive;
	
	void Awake () {
		spiral = transform.Find("Spiral").gameObject;
	}
	
	void Start () {
		isActive = true;
		RotateSprite();
	}
	
	void OnTriggerEnter (Collider col) {
		if(col.gameObject.tag == "Bounce Player") {
			Debug.Log(objPortalPair.GetComponent<BouncePortalController>().isActive);
			Debug.Log("Trigger Player");
			if(isActive && col.GetComponent<BouncePlayerController>().isActive) {
				Debug.Log("Trigger Player 2");
				AudioController.Play("Sound_Portal");
				col.transform.position = objPortalPair.transform.position;
				col.GetComponent<BouncePlayerController>().moveX = 0f;
				col.GetComponent<BouncePlayerController>().moveY = 0f;
				objPortalPair.GetComponent<BouncePortalController>().isActive = false;
			}
		}
		
		
	}
	
	void OnTriggerExit (Collider col) {
		//CHANGED add delay 2s
		Debug.Log("Delay 2 detik");
		StartCoroutine(DelayTime(0.5f));
//		if(col.gameObject.tag == "Bounce Player") {
//			if(!isActive) {
//				isActive = true;
//			}
//		}
	}
	
	IEnumerator DelayTime (float delay){
		yield return new WaitForSeconds(delay);
		isActive = true;
	}
	
	void RotateSprite () {
		iTween.RotateBy(spiral.gameObject, iTween.Hash("z", 1f, "time", 2f, "easetype", "linear", "oncomplete", "OnRotateSpriteComplete",
			"oncompletetarget", this.gameObject));
	}
	
	void OnRotateSpriteComplete () {
		RotateSprite();
	}
}
